import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GuardGUI {
	
	public GuardGUI(String name, final Counter counter) {
		JFrame frame = new JFrame();
		frame.setSize(500, 100);
		frame.setLocationRelativeTo(null); // places the windows in center
		frame.setTitle("Vagt : " + name);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel contain = new JPanel();
		JButton dec = new JButton("K�rer ind"); // Available parking slots decreases when car enters
		JButton inc = new JButton("K�rer ud"); // Available parking slots increases when car exits

		contain.add(inc);
		contain.add(dec);

		frame.add(contain);
		frame.setVisible(true);

		inc.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				counter.change(1);
			}
		});

		dec.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				counter.change(-1);
			}
		});
	}
}
