import javax.swing.Icon;
import java.awt.Graphics;
import java.awt.Component;
import java.util.ArrayList;

public class CompositeIcon implements Icon {
    private ArrayList<Icon> icons;

    public CompositeIcon() {
        icons = new ArrayList<Icon>();
    }
    
    /**
    *   @return Sum of all icons widths
    */
    public int getIconWidth() {
        int totalWidth = 0;
        for (Icon i : icons) {
            totalWidth += i.getIconWidth();
        }
        return totalWidth;
    }

    /**
    *           Because the icons only displays horizontal we only take 
    *           the height of the highest element.
    *   @return Height of the highest icon
    */
    public int getIconHeight() {
        int height = 0;
        for (Icon i : icons) {
            if (i.getIconHeight() > height) {
                height = i.getIconHeight();
            }
        }
        return height;
    }

    /**
    *           Adds an icon to CompositeIcon (this class)
     * @param   The element you want to add
     */
    public void addIcon(Icon ic) {
        icons.add(ic);
    }
    
    /**
     * @param i specifies the index of the Icon in the ArrayList
     * @return returns the icon at the specified index.
     */
    public Icon getIcon(int i) {
    	return icons.get(i);
    }
    
    /**
     *      Draws all of the icons in associated with CompositeIcon
     */
    public void paintIcon(Component c, Graphics g, int x, int y) {
        for (Icon i : icons) {
            i.paintIcon(c, g, x, y);
            x += i.getIconWidth();
        }
    }
}