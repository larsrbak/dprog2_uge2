import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Driver extends JFrame {
	public static void main(String[] args) {
		new Driver();
	}

	public Driver() {
		this.setSize(500,100);
		this.setLocationRelativeTo(null); // places the windows in center
		this.setTitle("dProg2 - aflevering 2");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel container = new JPanel();
		
		CompositeIcon btnIcons = new CompositeIcon();
		btnIcons.addIcon(new SquareIcon(20, Color.RED));
		btnIcons.addIcon(new SquareIcon(20, Color.GREEN));
		btnIcons.addIcon(new SquareIcon(20, Color.BLUE));
		
		JButton bRed = new JButton("Add red icon", btnIcons.getIcon(0));
		JButton bGreen = new JButton("Add green icon", btnIcons.getIcon(1));
		JButton bBlue = new JButton("Add blue icon", btnIcons.getIcon(2));

		final CompositeIcon box = new CompositeIcon();		
		JLabel l = new JLabel(box);
		
		container.add(bRed);
		container.add(bGreen);
		container.add(bBlue);
		container.add(l);
		
		this.add(container);
		this.setVisible(true);

		btnListener(bRed, Color.RED, box);
		btnListener(bGreen, Color.GREEN, box);
		btnListener(bBlue, Color.BLUE, box);
	}

	public void btnListener(JButton b, final Color c, final CompositeIcon box) {
		b.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				box.addIcon(new SquareIcon(20, c));
				repaintIcon();			
			}
		});
	}

	public void repaintIcon() {
		this.pack();
		this.repaint();
	}
}