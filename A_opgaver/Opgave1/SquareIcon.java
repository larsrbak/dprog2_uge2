import javax.swing.Icon;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Component;

public class SquareIcon implements Icon {
	private int size;
	private Color color;

	public SquareIcon(int size, Color color) { 
		this.size = size;
		this.color = color;
	}

	public int getIconHeight() {
		return size;
	}
	
	public int getIconWidth() {
		return size;
	}
	
	public void paintIcon(Component c, Graphics g, int x, int y) {
		g.setColor(color);
        g.fillRect(0, 0, size, size);
	}
}