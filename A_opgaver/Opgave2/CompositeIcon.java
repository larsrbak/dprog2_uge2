import javax.swing.Icon;
import java.awt.Graphics;
import java.awt.Component;
import java.util.ArrayList;

public class CompositeIcon implements Icon {
        private ArrayList<Icon> icons;

        public CompositeIcon() {
                icons = new ArrayList<Icon>();
        }

        public int getIconWidth() {
                int totalWidth = 0;
                for (Icon i : icons) {
                        totalWidth += i.getIconWidth();
                }
                return totalWidth;
        }

        public int getIconHeight() {
                int totalHeight = 0;
                for (Icon i : icons) {
                        totalHeight += i.getIconHeight();
                }
                return totalHeight;
        }

        public void addIcon(Icon ic) {
                icons.add(ic);
        }

        public void addIcon(CompositeIcon ci) {
                icons.addAll(ci.icons);
        }

        public void paintIcon(Component c, Graphics g, int x, int y) {
                int offset = x;
                for (Icon i : icons) {
                        i.paintIcon(c, g, offset, y);
                        offset += i.getIconWidth();
                }
        }
}