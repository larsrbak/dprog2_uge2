Følgende klasse anvender ovenstående klasser
public class MileageSummary {

    private double totalMileage = 0.0;

    public void addMileage(Truck v) {
        totalMileage += v.getMileage();
    }

    public void addMileage(SolarCar v) {
        totalMileage += v.getMileage();
    }

    public void addMileage(HorseCarriage v) {
        totalMileage += v.getMileage();
    }

    public double getSummary() {
        return totalMileage;
    }
}
Bemærk at metode addMileage er overloaded, idet den er implementeret flere gange med forskellige signaturer (forskellige lister af parametertyper). I dette tilfælde er koden i implementationerne næsten ens. Hvilke ændringer er nødvendige, hvis du vil undgå at skrive næsten identisk kode flere gange i klassen MileageSummary?