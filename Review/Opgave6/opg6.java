Ved et parkeringsanlæg har en vagt et betjeningspanel GuardGUI med to knapper ``Bil kører ind'' og ``bil kører ud'', f.eks. http://cs.au.dk/~gudmund/dprog2_2013/GuardGUI.png. Ved indfaldsvejene til centrum er der skilte RoadGUI med tekst på formen ``800 ledige P-pladser'', f.eks. http://cs.au.dk/~gudmund/dprog2_2013/RoadGUI.png. En klasse Counter (se nedenfor) kan holde styr på antallet af biler. Forklar hvorledes klasserne GuardGUI, RoadGUI og Counter kan samarbejde ifølge Observer mønstret (og Model-View-Controller). Tegn et UML diagram, der illustrerer klassernes samarbejde.

import java.util.*;
import javax.swing.event.*;

public class Counter {
	
    private int value;
    
    private ArrayList<ChangeListener> listeners =
	new ArrayList<ChangeListener>();
    
    public Counter(int initialValue) {
	value = initialValue;
    }
    
    public void change(int diff) {
	value += diff;
	for (ChangeListener listener: listeners)
	    listener.stateChanged(new ChangeEvent(this));
    }
    
    public int getValue() { return value; }
    
    public void addChangeListener(ChangeListener l) {
	listeners.add(l);
    }
}