public interface Drivable {
    public boolean drive(int distance);
    public double getMileage();
}

public class Truck implements Drivable {
    private double mileage = 0.0;
    private double fuel = 0.0;
    private double kmPrLiter = 5.0;
    
    public boolean drive(int distance) {
        double fuelUsage = distance/kmPrLiter;
        if (fuelUsage <= fuel) {
            fuel -= fuelUsage;
            mileage += distance;
            return true;
        }        
        else return false;
    }

    public double getMileage() {
        return mileage;
    }

    public void fill(int i) {
        fuel += i;
    }
}

public class SolarCar implements Drivable {
    private double mileage = 0.0;

    public boolean drive(int distance) {
        mileage +=distance;
        return true;
    }

    public double getMileage() {
        return mileage;
    }
}

public class HorseCarriage {
    private double mileage = 0.0;
    private double hoursLeft = 0.0;
    private double kmPrHour = 10.0;
    
    public boolean drive(int distance) {
        double hoursNeeded = distance/kmPrHour;
        if (hoursNeeded <= hoursLeft) {
            hoursLeft -= hoursNeeded;
            mileage += distance;
            return true;
        }        
        else return false;
    }

    public double getMileage() {
        return mileage;
    }

    public void restHorses() {
        hoursLeft = 10;
    }
}
Hvilke af følgende sætninger er legale?

Drivable v1 = new Drivable();
Drivable v2 = new Truck();
Drivable v3 = new SolarCar();
Drivable v4 = new HorseCarriage();

v2.fill(10);
v2.drive(20);
v2.getMileage();
Forklar hvorledes polymorfi anvendes i sætningerne.