Betragt følgende klasser og interfaces

public interface Box {
    String getContent();
}

public class SimpleBox implements Box {
    private String content;
    public SimpleBox(String content) {
        this.content = content;
    }
    public String getContent() {
        return content;
    }
}

public class CompositeBox implements Box {
    private List<Box> boxes = new ArrayList<Box>();
    public String getContent() {
        String content = "";
        for (Box b : boxes) {
            content += " "+ b.getContent();
        }
        return content;
    }
    public void addBox(Box b) {
        boxes.add(b);
    }
}
Tegn et UML diagram, der viser sammenhængen mellem klasserne og interfacet.

Hvilket mønster er på spil (forklar) ?

Er følgende driver program legalt (hvorfor / hvorfor ikke) ?

Hvad udskrives ved linjerne /*1*/, /*2*/ og /*3*/ ?

public class Driver {
    public static void main(String[] args) {
        Box box1 = new SimpleBox("Noodles");
        Box box2 = new SimpleBox("Carrots");
        Box box3 = new Box() {
                 public String getContent() {
                    return "Chicken";
                }
            };
        CompositeBox meal1 = new CompositeBox();
        meal1.addBox(box1);
        meal1.addBox(box2);
        CompositeBox meal2 = new CompositeBox();
        meal2.addBox(meal1);
        meal2.addBox(box3);
        /*1*/ System.out.println(meal2.getContent());
        meal2.addBox(meal1);
        /*2*/ System.out.println(meal2.getContent());
        meal2.addBox(meal2);
        /*3*/ System.out.println(meal2.getContent());
    }
}