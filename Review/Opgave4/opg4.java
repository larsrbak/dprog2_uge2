Betragt følgende klasse T med en indre klasse C. Hvilke variabler kan tilgås fra f metoden?
public class T {
    public void m(final int x, int y) {
        int a = 1;
        final int b = 2;;

        class C {
            public void f() {  ...  }
        }

        final int c = 3;
      ...
    }
    private int t;
}