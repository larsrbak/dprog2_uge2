import javax.swing.JFrame;	
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class RoadGUI implements ChangeListener {
	private Counter counter;
	private JLabel msg;

	public RoadGUI(int initialslots, Counter counter) {
		JFrame frame = new JFrame();
		this.counter = counter;

		frame.setSize(500, 100);
		frame.setLocationRelativeTo(null); // places the windows in center
		frame.setTitle("ROAD GUI");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel contain = new JPanel();
		msg = new JLabel("Frie pladser:" + initialslots);

		contain.add(msg);
		frame.add(contain);
		frame.setVisible(true);
	}

	public void stateChanged(ChangeEvent e) {
		msg.setText("Frie pladser:" + counter.getValue());
	}
}