public class ParkingTest {

	public static void main(String[] args) {
		new ParkingTest();
	}

	public ParkingTest() {
		Counter counter = new Counter(100);

		RoadGUI r1 = new RoadGUI(counter.getValue(), counter);
		RoadGUI r2 = new RoadGUI(counter.getValue(), counter);

		counter.addChangeListener(r1);
		counter.addChangeListener(r2);

		new GuardGUI("Lars", counter);
		new GuardGUI("Kristian", counter);
	}
}
