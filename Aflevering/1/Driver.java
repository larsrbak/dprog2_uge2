import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Driver extends JFrame {
	
	public static void main(String[] args) {
		new Driver();
	}

	public Driver() {
		this.setSize(500,100);
		this.setLocationRelativeTo(null); // places the windows in center
		this.setTitle("dProg2 - aflevering 2");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel container = new JPanel();
		
		CompositeIcon BtnIcons = new CompositeIcon();
		BtnIcons.addIcon(new SquareIcon(20, Color.RED));
		BtnIcons.addIcon(new SquareIcon(20, Color.GREEN));
		BtnIcons.addIcon(new SquareIcon(20, Color.BLUE));
		
		JButton bRed = new JButton("Add red icon", BtnIcons.getIcon(0));
		JButton bGreen = new JButton("Add green icon", BtnIcons.getIcon(1));
		JButton bBlue = new JButton("Add blue icon", BtnIcons.getIcon(2));
		
		final CompositeIcon box = new CompositeIcon();		
		JLabel l = new JLabel(box);
		
		container.add(bRed);
		container.add(bGreen);
		container.add(bBlue);
		container.add(l);
		
		this.add(container);
		this.setVisible(true);
		
		bRed.addActionListener(new ActionListener()  {
			
			public void actionPerformed(ActionEvent arg0) {
				box.addIcon(new SquareIcon(20, Color.RED));
				repaintIcon();
			}
		});
		
		bGreen.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				box.addIcon(new SquareIcon(20, Color.GREEN));
				repaintIcon();
			}
		});
		
		bBlue.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				box.addIcon(new SquareIcon(20, Color.BLUE));
				repaintIcon();
			}
		});
	}

	public void repaintIcon() {
		this.pack();
		this.repaint();
	}
}