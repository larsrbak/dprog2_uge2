import javax.swing.Icon;
import java.awt.Graphics;
import java.awt.Component;
import java.util.ArrayList;

public class CompositeIcon implements Icon {
    private ArrayList<Icon> icons;

    public CompositeIcon() {
        icons = new ArrayList<Icon>();
    }

    public int getIconWidth() {
        int totalWidth = 0;
        for (Icon i : icons) {
            totalWidth += i.getIconWidth();
        }
        return totalWidth;
    }

    public int getIconHeight() {
        /*
         * Vi er kun intresseret i at firkanterne skrives horisontalt. 
         * Derfor skal det compositeIcon omr�de ikke v�re summen af alle ikonerne.
         * Her finder denne return metode den Icon med st�rste h�jde.
         */
        int height = 0;
        for (Icon i : icons) {
            if (i.getIconHeight() > height) {
                height = i.getIconHeight();
            }
        }
        return height;
    }

    public void addIcon(Icon ic) {
        icons.add(ic);
    }
        
    /**
     * @param i specifies the index of the Icon in the ArrayList
     * @return returns the icon at the specified index.
     */
    public Icon getIcon(int i) {
    	return icons.get(i);
    }
    
    public void paintIcon(Component c, Graphics g, int x, int y) {
        int offset = x;
        for (Icon i : icons) {
            i.paintIcon(c, g, offset, y);
            offset += i.getIconWidth();
        }
    }
}